drop table T_USER if exists;
create table T_USER
(
    ID         integer generated by default as identity (start with 1, increment by 1) primary key,
    NAME       varchar(50) not null,
    SURNAME    varchar(50) not null,
    ACCOUNT_ID integer default 0
);

drop table T_ACCOUNT if exists;
create table T_ACCOUNT
(
    ID      integer generated by default as identity (start with 1, increment by 1) primary key,
    USER_ID integer not null
)