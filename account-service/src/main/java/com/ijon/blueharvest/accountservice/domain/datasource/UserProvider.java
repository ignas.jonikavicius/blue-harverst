package com.ijon.blueharvest.accountservice.domain.datasource;

import com.ijon.blueharvest.accountservice.data.repository.UserRepository;
import com.ijon.blueharvest.accountservice.data.repository.UserSource;
import com.ijon.blueharvest.accountservice.domain.model.UserModel;

import java.util.Arrays;
import java.util.List;

public class UserProvider {
    private static final List<UserDataSource> SOURCES = Arrays.asList(new UserSource());

    public UserProvider() {
    }

    /* Temporary solution for @Autowired Repository */
    public void addRepository(UserRepository repository) {
        for (UserDataSource source : SOURCES) {
            if (source instanceof UserSource) {
                ((UserSource) source).addRepository(repository);
            }
        }
    }

    /**
     * Sets the new account ID for given user in repository
     *
     * @param userId       Id of the user to update
     * @param newAccountId New account ID
     * @return The updated account ID
     */
    public Integer setAccountId(Integer userId, Integer newAccountId) {
        for (UserDataSource s : SOURCES) {
            Integer result = s.setAccountId(userId, newAccountId);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }

    /**
     * Returns a list of all Users in repository
     *
     * @return List of Users
     */
    public List<UserModel> getAllUsers() {
        for (UserDataSource s : SOURCES) {
            List<UserModel> result = s.findAllUsers();
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }
}
