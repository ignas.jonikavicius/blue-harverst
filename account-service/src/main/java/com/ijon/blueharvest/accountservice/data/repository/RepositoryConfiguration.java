package com.ijon.blueharvest.accountservice.data.repository;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;

@Configuration
@EntityScan("com.ijon.blueharvest.accountservice.data.repository")
@EnableJpaRepositories("com.ijon.blueharvest.accountservice.data.repository")
@PropertySource("classpath:db-config.properties")
public class RepositoryConfiguration {

    @Bean
    public DataSource dataSource() {
        DataSource dataSource = (new EmbeddedDatabaseBuilder()).addScript("classpath:db/schema.sql")
                .addScript("classpath:db/data.sql").build();

        return dataSource;
    }
}
