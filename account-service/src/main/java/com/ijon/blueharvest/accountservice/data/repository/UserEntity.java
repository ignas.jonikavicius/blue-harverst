package com.ijon.blueharvest.accountservice.data.repository;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_USER")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    protected String name;
    protected String surname;
    protected Integer accountId;


    /**
     * Default constructor for JPA only.
     */
    protected UserEntity() {
    }

    public UserEntity(String name, String surname, Integer accountId) {
        this.name = name;
        this.surname = surname;
        this.accountId = accountId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
