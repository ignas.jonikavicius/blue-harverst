package com.ijon.blueharvest.accountservice.domain.datasource;

import com.ijon.blueharvest.accountservice.domain.model.AccountModel;

import java.util.List;

public interface AccountDataSource {

    /**
     * Stores Account data to repository and returns id of the new entry
     *
     * @param account Data to store
     * @return Id of the stored data
     */
    Integer createAccount(AccountModel account);

    /**
     * Returns a list of Accounts from repository filtered by user ID
     *
     * @param userId Filter value
     * @return List of Accounts
     */
    List<AccountModel> findAccountsByUserId(Integer userId);
}
