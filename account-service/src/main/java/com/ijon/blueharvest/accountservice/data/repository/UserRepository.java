package com.ijon.blueharvest.accountservice.data.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    /**
     * Returns a list of all Users in repository
     *
     * @return List of Users
     */
    public List<UserEntity> findAll();

    /**
     * Returns a User for ID
     * @param id Search value
     * @return User
     */
    public UserEntity findById(Integer id);

}
