package com.ijon.blueharvest.accountservice.domain.commands;

import com.ijon.blueharvest.accountservice.data.repository.UserRepository;
import com.ijon.blueharvest.accountservice.domain.datasource.UserProvider;
import com.ijon.blueharvest.accountservice.domain.model.UserModel;

import java.util.List;

/**
 * Returns a list of Users
 */
public class RequestUsersCommand implements Command {

    private final UserProvider provider = new UserProvider();

    public RequestUsersCommand() {
    }

    /* Temporary solution for @Autowired Repository */
    public RequestUsersCommand addRepository(UserRepository repository) {
        provider.addRepository(repository);

        return this; // for chaining
    }

    @Override
    public List<UserModel> execute() {
        return provider.getAllUsers();
    }
}
