package com.ijon.blueharvest.accountservice.domain.datasource;

import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;

import java.util.List;

public class TransactionProvider {

    private List<TransactionDataSource> SOURCES;

    public TransactionProvider(List<TransactionDataSource> list) {
        this.SOURCES = list;
    }

    /**
     * Stores Transaction data to repository
     *
     * @param transaction Data to store
     * @return Success state
     */
    public Boolean createTransaction(TransactionModel transaction) {
        for (TransactionDataSource s : SOURCES) {
            Boolean result = s.createTransaction(transaction);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }

    /**
     * Returns a list of Transactions from repository filtered by account ID
     *
     * @param accountId Filter value
     * @return List of Transactions
     */
    public List<TransactionModel> getTransactions(Integer accountId) {
        for (TransactionDataSource s : SOURCES) {
            List<TransactionModel> result = s.getTransactionsForAccount(accountId);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }
}
