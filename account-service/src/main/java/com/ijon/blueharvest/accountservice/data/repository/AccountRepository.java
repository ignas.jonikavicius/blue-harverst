package com.ijon.blueharvest.accountservice.data.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<AccountEntity, Long> {

    /**
     * Returns a list of Accounts from repository filtered by user ID
     *
     * @param userId Filter value
     * @return List of Accounts
     */
    List<AccountEntity> findAllByUserIdEquals(Integer userId);
}
