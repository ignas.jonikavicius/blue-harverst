package com.ijon.blueharvest.accountservice.domain.commands;

import com.ijon.blueharvest.accountservice.data.repository.AccountRepository;
import com.ijon.blueharvest.accountservice.domain.datasource.AccountProvider;
import com.ijon.blueharvest.accountservice.domain.model.AccountModel;

/**
 * Stores a new Account to repository
 */
public class CreateAccountCommand implements Command {

    private AccountModel accountModel;
    private final AccountProvider provider = new AccountProvider();

    public CreateAccountCommand(AccountModel accountModel) {
        this.accountModel = accountModel;
    }

    /* Temporary solution for @Autowired Repository */
    public CreateAccountCommand addRepository(AccountRepository repository) {
        provider.addRepository(repository);

        return this; // for chaining
    }

    @Override
    public Integer execute() {
        return provider.createAccount(this.accountModel);
    }
}
