package com.ijon.blueharvest.accountservice.controller;

import com.ijon.blueharvest.accountservice.data.repository.AccountRepository;
import com.ijon.blueharvest.accountservice.data.repository.UserRepository;
import com.ijon.blueharvest.accountservice.data.service.TransactionService;
import com.ijon.blueharvest.accountservice.domain.commands.RequestAccountsByUserIdCommand;
import com.ijon.blueharvest.accountservice.domain.commands.RequestTransactionsByAccountIdCommand;
import com.ijon.blueharvest.accountservice.domain.commands.RequestUsersCommand;
import com.ijon.blueharvest.accountservice.domain.model.AccountModel;
import com.ijon.blueharvest.accountservice.domain.model.OverviewModel;
import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;
import com.ijon.blueharvest.accountservice.domain.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/overview")
public class OverviewController {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected AccountRepository accountRepository;

    @Autowired
    protected TransactionService transactionService;

    @GetMapping("")
    public OverviewModel getOverview() {
        OverviewModel overview = new OverviewModel();

        List<UserModel> users = new RequestUsersCommand().addRepository(userRepository).execute();
        overview.addUsers(users);

        for (UserModel user : users) {
            List<AccountModel> accounts = new RequestAccountsByUserIdCommand(user.getId()).addRepository(accountRepository).execute();
            overview.addAccounts(user.getId(), accounts);

            for (AccountModel account : accounts) {
                List<TransactionModel> transactions = new RequestTransactionsByAccountIdCommand(account.getId(), Arrays.asList(transactionService)).execute();
                overview.addTransactions(account.getId(), transactions);

                for (TransactionModel transaction : transactions) {
                    user.addBalance(transaction.getValue());
                }
            }
        }

        return overview;
    }

}
