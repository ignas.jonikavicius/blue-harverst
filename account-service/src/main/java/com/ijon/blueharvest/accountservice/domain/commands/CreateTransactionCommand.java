package com.ijon.blueharvest.accountservice.domain.commands;

import com.ijon.blueharvest.accountservice.domain.datasource.TransactionDataSource;
import com.ijon.blueharvest.accountservice.domain.datasource.TransactionProvider;
import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;

import java.util.List;

/**
 * Stores a new Transaction to repository
 */
public class CreateTransactionCommand implements Command {

    private TransactionModel transaction;
    private TransactionProvider provider;

    @Override
    public Boolean execute() {
        return provider.createTransaction(this.transaction);
    }

    public CreateTransactionCommand(TransactionModel transaction, List<TransactionDataSource> sources) {
        this.transaction = transaction;
        this.provider = new TransactionProvider(sources);
    }
}
