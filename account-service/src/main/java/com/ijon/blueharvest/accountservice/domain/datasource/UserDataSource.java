package com.ijon.blueharvest.accountservice.domain.datasource;

import com.ijon.blueharvest.accountservice.domain.model.UserModel;

import java.util.List;

public interface UserDataSource {
    /**
     * Sets the new account ID for given user in repository
     *
     * @param userId       Id of the user to update
     * @param newAccountId New account ID
     * @return The updated account ID
     */
    Integer setAccountId(Integer userId, Integer newAccountId);

    /**
     * Returns a list of all Users in repository
     *
     * @return List of Users
     */
    List<UserModel> findAllUsers();
}
