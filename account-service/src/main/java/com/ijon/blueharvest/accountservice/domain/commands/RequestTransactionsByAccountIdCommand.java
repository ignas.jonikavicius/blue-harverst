package com.ijon.blueharvest.accountservice.domain.commands;

import com.ijon.blueharvest.accountservice.domain.datasource.TransactionDataSource;
import com.ijon.blueharvest.accountservice.domain.datasource.TransactionProvider;
import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;

import java.util.List;

/**
 * Returns a list of Transactions filtered by account ID
 */
public class RequestTransactionsByAccountIdCommand implements Command {

    private Integer accountId;
    private TransactionProvider provider;

    public RequestTransactionsByAccountIdCommand(Integer accountId, List<TransactionDataSource> sources) {
        this.accountId = accountId;
        this.provider = new TransactionProvider(sources);
    }

    @Override
    public List<TransactionModel> execute() {
        return provider.getTransactions(this.accountId);
    }
}
