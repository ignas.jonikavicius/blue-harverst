package com.ijon.blueharvest.accountservice.domain.commands;

public interface Command {
    Object execute();
}
