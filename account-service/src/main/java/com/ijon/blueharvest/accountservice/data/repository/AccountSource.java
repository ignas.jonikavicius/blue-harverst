package com.ijon.blueharvest.accountservice.data.repository;

import com.ijon.blueharvest.accountservice.domain.datasource.AccountDataSource;
import com.ijon.blueharvest.accountservice.domain.model.AccountModel;

import java.util.List;
import java.util.stream.Collectors;

public class AccountSource implements AccountDataSource {

    protected AccountRepository accountRepository;

    /* Temporary solution for @Autowired Repository */
    public void addRepository(AccountRepository repository) {
        this.accountRepository = repository;
    }

    @Override
    public Integer createAccount(AccountModel account) {
        return accountRepository.save(DataMapper.toData(account)).getId();
    }

    @Override
    public List<AccountModel> findAccountsByUserId(Integer userId) {
        return accountRepository.findAllByUserIdEquals(userId).stream()
                .map(DataMapper::toDomain)
                .collect(Collectors.toList());
    }
}
