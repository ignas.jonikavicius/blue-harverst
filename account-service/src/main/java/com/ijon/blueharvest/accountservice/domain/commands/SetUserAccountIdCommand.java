package com.ijon.blueharvest.accountservice.domain.commands;

import com.ijon.blueharvest.accountservice.data.repository.UserRepository;
import com.ijon.blueharvest.accountservice.domain.datasource.UserProvider;

/**
 * Updates User's account ID
 */
public class SetUserAccountIdCommand implements Command {

    private Integer userId;
    private Integer newAccountId;
    private final UserProvider provider = new UserProvider();

    public SetUserAccountIdCommand(Integer userId, Integer newAccountId) {
        this.userId = userId;
        this.newAccountId = newAccountId;
    }

    /* Temporary solution for @Autowired Repository */
    public SetUserAccountIdCommand addRepository(UserRepository repository) {
        provider.addRepository(repository);

        return this; // for chaining
    }

    @Override
    public Integer execute() {
        return provider.setAccountId(this.userId, this.newAccountId);
    }
}
