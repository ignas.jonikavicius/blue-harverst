package com.ijon.blueharvest.accountservice.domain.datasource;

import com.ijon.blueharvest.accountservice.data.repository.AccountRepository;
import com.ijon.blueharvest.accountservice.data.repository.AccountSource;
import com.ijon.blueharvest.accountservice.domain.model.AccountModel;

import java.util.Arrays;
import java.util.List;

public class AccountProvider {

    private static final List<AccountDataSource> SOURCES = Arrays.asList(new AccountSource());

    public AccountProvider() {
    }

    /* Temporary solution for @Autowired Repository */
    public void addRepository(AccountRepository repository) {
        for (AccountDataSource source : SOURCES) {
            if (source instanceof AccountSource) {
                ((AccountSource) source).addRepository(repository);
            }
        }
    }

    /**
     * Stores Account data to repository and returns id of the new entry
     *
     * @param account Data to store
     * @return Id of the stored data
     */
    public Integer createAccount(AccountModel account) {
        for (AccountDataSource s : SOURCES) {
            Integer result = s.createAccount(account);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }

    /**
     * Returns a list of Accounts from repository filtered by user ID
     *
     * @param userId Filter value
     * @return List of Accounts
     */
    public List<AccountModel> getAccountsForUser(Integer userId) {
        for (AccountDataSource s : SOURCES) {
            List<AccountModel> result = s.findAccountsByUserId(userId);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }
}
