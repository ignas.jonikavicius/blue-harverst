package com.ijon.blueharvest.accountservice.domain.datasource;

import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;

import java.util.List;

public interface TransactionDataSource {

    /**
     * Stores Transaction data to repository
     *
     * @param transaction Data to store
     * @return Success state
     */
    Boolean createTransaction(TransactionModel transaction);

    /**
     * Returns a list of Transactions from repository filtered by account ID
     *
     * @param accountId Filter value
     * @return List of Transactions
     */
    List<TransactionModel> getTransactionsForAccount(Integer accountId);
}
