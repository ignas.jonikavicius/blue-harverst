package com.ijon.blueharvest.accountservice.data.repository;

import com.ijon.blueharvest.accountservice.domain.model.AccountModel;
import com.ijon.blueharvest.accountservice.domain.model.UserModel;

/**
 * Maps Domain Models to Repository Entities, and vice versa
 */
public class DataMapper {

    public static AccountModel toDomain(AccountEntity entity) {
        return new AccountModel(entity.getId(), entity.getUserId(), 0.0);
    }

    public static AccountEntity toData(AccountModel model) {
        return new AccountEntity(model.getUserId());
    }

    public static UserModel toDomain(UserEntity entity) {
        return new UserModel(entity.getId(), entity.getName(), entity.getSurname(), entity.getAccountId());
    }

    public static UserEntity toData(UserModel model) {
        return new UserEntity(model.getName(), model.getSurname(), model.getAccountId());
    }
}
