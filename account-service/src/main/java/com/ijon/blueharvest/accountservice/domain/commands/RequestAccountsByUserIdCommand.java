package com.ijon.blueharvest.accountservice.domain.commands;

import com.ijon.blueharvest.accountservice.data.repository.AccountRepository;
import com.ijon.blueharvest.accountservice.domain.datasource.AccountProvider;
import com.ijon.blueharvest.accountservice.domain.model.AccountModel;

import java.util.List;

/**
 * Returns a list of Accounts filtered by user ID
 */
public class RequestAccountsByUserIdCommand implements Command {

    private Integer userId;
    private final AccountProvider provider = new AccountProvider();

    public RequestAccountsByUserIdCommand(Integer userId) {
        this.userId = userId;
    }

    /* Temporary solution for @Autowired Repository */
    public RequestAccountsByUserIdCommand addRepository(AccountRepository repository) {
        provider.addRepository(repository);

        return this; // for chaining
    }

    @Override
    public List<AccountModel> execute() {
        return provider.getAccountsForUser(this.userId);
    }
}
