package com.ijon.blueharvest.accountservice.data.service;

import com.ijon.blueharvest.accountservice.domain.datasource.TransactionDataSource;
import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;


@Service
public class TransactionService implements TransactionDataSource {

    public static final String TRANSACTION_SERVICE_URL = "http://transaction-service";

    @Autowired
    @LoadBalanced
    protected RestTemplate restTemplate;

    @Override
    public Boolean createTransaction(TransactionModel transaction) {
        HttpEntity<TransactionModel> request = new HttpEntity<>(transaction);
        ResponseEntity<String> uri = restTemplate.exchange(TRANSACTION_SERVICE_URL + "/transactions",
                HttpMethod.POST, request, String.class);

        return uri.getStatusCode() == HttpStatus.CREATED;
    }

    @Override
    public List<TransactionModel> getTransactionsForAccount(Integer accountId) {
        ResponseEntity<TransactionModel[]> response = restTemplate.getForEntity(TRANSACTION_SERVICE_URL + "/transactions/account/" + accountId, TransactionModel[].class);
        List<TransactionModel> list = Arrays.asList(response.getBody());
        return list;
    }
}