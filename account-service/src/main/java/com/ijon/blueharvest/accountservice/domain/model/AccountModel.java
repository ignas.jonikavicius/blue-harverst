package com.ijon.blueharvest.accountservice.domain.model;

public class AccountModel {

    private Integer id;
    private Integer userId;
    private Double initialCredit;

    public AccountModel() {
    }

    public AccountModel(Integer id, Integer userId, Double initialCredit) {
        this.id = id;
        this.userId = userId;
        this.initialCredit = initialCredit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getInitialCredit() {
        return initialCredit;
    }

    public void setInitialCredit(Double initialCredit) {
        this.initialCredit = initialCredit;
    }
}
