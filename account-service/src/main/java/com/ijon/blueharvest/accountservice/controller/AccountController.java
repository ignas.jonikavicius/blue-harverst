package com.ijon.blueharvest.accountservice.controller;

import com.ijon.blueharvest.accountservice.data.service.TransactionService;
import com.ijon.blueharvest.accountservice.domain.commands.CreateAccountCommand;
import com.ijon.blueharvest.accountservice.domain.commands.CreateTransactionCommand;
import com.ijon.blueharvest.accountservice.domain.commands.SetUserAccountIdCommand;
import com.ijon.blueharvest.accountservice.domain.model.AccountModel;
import com.ijon.blueharvest.accountservice.data.repository.AccountRepository;
import com.ijon.blueharvest.accountservice.data.repository.UserRepository;
import com.ijon.blueharvest.accountservice.domain.model.TransactionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected AccountRepository accountRepository;

    @Autowired
    protected TransactionService transactionService;

    @PostMapping("")
    public ResponseEntity<Void> createAccount(@RequestBody AccountModel account) {
        // TODO : add @RequestBody validation
        // TODO : add success checks with possibility to rollback on failure
        Integer newAccountId = new CreateAccountCommand(account).addRepository(accountRepository).execute();
        new SetUserAccountIdCommand(account.getUserId(), newAccountId).addRepository(userRepository).execute();

        if (account.getInitialCredit() != 0.0)
            new CreateTransactionCommand(new TransactionModel(0, newAccountId, account.getInitialCredit()), Arrays.asList(transactionService)).execute();

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
