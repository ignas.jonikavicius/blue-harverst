package com.ijon.blueharvest.accountservice.domain.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OverviewModel {

    private List<UserModel> users = new ArrayList<>();
    private HashMap<Integer, List<AccountModel>> usersAccounts = new HashMap<>();
    private HashMap<Integer, List<TransactionModel>> accountsTransactions = new HashMap<>();

    public OverviewModel() {
    }

    public OverviewModel(List<UserModel> users, HashMap<Integer, List<AccountModel>> usersAccounts, HashMap<Integer, List<TransactionModel>> accountsTransactions) {
        this.users = users;
        this.usersAccounts = usersAccounts;
        this.accountsTransactions = accountsTransactions;
    }

    public void addUsers(List<UserModel> users) {
        this.users.addAll(users);
    }

    public void addAccounts(Integer key, List<AccountModel> value) {
        this.usersAccounts.put(key, value);
    }

    public void addTransactions(Integer key, List<TransactionModel> value) {
        this.accountsTransactions.put(key, value);
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public HashMap<Integer, List<AccountModel>> getUsersAccounts() {
        return usersAccounts;
    }

    public void setUsersAccounts(HashMap<Integer, List<AccountModel>> usersAccounts) {
        this.usersAccounts = usersAccounts;
    }

    public HashMap<Integer, List<TransactionModel>> getAccountsTransactions() {
        return accountsTransactions;
    }

    public void setAccountsTransactions(HashMap<Integer, List<TransactionModel>> accountsTransactions) {
        this.accountsTransactions = accountsTransactions;
    }

}
