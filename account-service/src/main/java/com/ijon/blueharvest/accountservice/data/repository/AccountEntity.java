package com.ijon.blueharvest.accountservice.data.repository;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_ACCOUNT")
public class AccountEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    protected Integer userId;

    /**
     * Default constructor for JPA only.
     */
    protected AccountEntity() {
    }

    public AccountEntity(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
