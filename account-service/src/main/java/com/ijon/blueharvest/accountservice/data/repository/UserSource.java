package com.ijon.blueharvest.accountservice.data.repository;

import com.ijon.blueharvest.accountservice.domain.datasource.UserDataSource;
import com.ijon.blueharvest.accountservice.domain.model.UserModel;

import java.util.List;
import java.util.stream.Collectors;

public class UserSource implements UserDataSource {

    protected UserRepository userRepository;

    /* Temporary solution for @Autowired Repository */
    public void addRepository(UserRepository repository) {
        this.userRepository = repository;
    }

    @Override
    public Integer setAccountId(Integer userId, Integer newAccountId) {
        UserEntity user = userRepository.findById(userId);
        user.setAccountId(newAccountId);
        return userRepository.save(user).getAccountId();
    }

    @Override
    public List<UserModel> findAllUsers() {
        return userRepository.findAll().stream()
                .map(DataMapper::toDomain)
                .collect(Collectors.toList());
    }
}
