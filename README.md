# Back-End Coding Assignment for Blue Harvest

This is the back-end coding assignment for Blue Harvest, by Ignas Jonikavicius.

## Structure

The project contains 4 applications that have to be built and run separately

```
.
|-- eureka-server
|-- transaction-service
|-- account-service
|-- web-service
``` 

## Build and Run

Maven is used to build the applications.

### Procedure

To run the microservices system from the command-line, open four CMD windows (Windows) or four Terminal windows (MacOS, Linux).

1. In first window change the directory to `./eureka-server/`
1. In the same window, build the application using `mvn package`
1. In the same window run: `java -jar target/eureka-server-0.0.1-SNAPSHOT.jar`
1. In second window change the directory to `./transaction-service/`
1. In the same window, build the application using `mvn package`
1. In the same window run: `java -jar target/transaction-service-0.0.1-SNAPSHOT.jar`
1. In third window change the directory to `./account-service/`
1. In the same window, build the application using `mvn package`
1. In the same window run: `java -jar target/account-service-0.0.1-SNAPSHOT.jar`
1. In fourth window change the directory to `./web-service/`
1. In the same window, build the application using `mvn package`
1. In the same window run: `java -jar target/web-service-0.0.1-SNAPSHOT.jar`

## Front-end

Access frontend with [http://localhost:8050](http://localhost:8050)

## Todo

* Unit testing
* Exception handling
* Data validation
* Logging