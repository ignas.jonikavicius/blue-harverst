package com.ijon.blueharvest.transactionservice.domain.commands;

import com.ijon.blueharvest.transactionservice.data.repository.TransactionRepository;
import com.ijon.blueharvest.transactionservice.domain.datasource.TransactionProvider;
import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;

import java.util.List;

/**
 * Returns a list of Transactions filtered by account ID
 */
public class RequestTransactionsByAccountIdCommand implements Command {

    private Integer accountId;
    private final TransactionProvider provider = new TransactionProvider();

    public RequestTransactionsByAccountIdCommand(Integer accountId) {
        this.accountId = accountId;
    }

    /* Temporary solution for @Autowired Repository */
    public RequestTransactionsByAccountIdCommand addRepository(TransactionRepository repository) {
        provider.addRepository(repository);

        return this; // for chaining
    }

    @Override
    public List<TransactionModel> execute() {
        return provider.getTransactionsForAccount(this.accountId);
    }
}
