package com.ijon.blueharvest.transactionservice.domain.commands;

import com.ijon.blueharvest.transactionservice.data.repository.TransactionRepository;
import com.ijon.blueharvest.transactionservice.domain.datasource.TransactionProvider;
import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;

/**
 * Stores a new Transaction to repository
 */
public class CreateTransactionCommand implements Command {

    private TransactionModel transactionModel;
    private final TransactionProvider provider = new TransactionProvider();

    public CreateTransactionCommand(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    /* Temporary solution for @Autowired Repository */
    public CreateTransactionCommand addRepository(TransactionRepository repository) {
        provider.addRepository(repository);

        return this; // for chaining
    }

    @Override
    public Integer execute() {
        return provider.createTransaction(this.transactionModel);
    }
}
