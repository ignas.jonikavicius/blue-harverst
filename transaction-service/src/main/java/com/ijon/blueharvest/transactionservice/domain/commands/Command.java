package com.ijon.blueharvest.transactionservice.domain.commands;

public interface Command {
    Object execute();
}
