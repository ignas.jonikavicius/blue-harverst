package com.ijon.blueharvest.transactionservice.data.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<TransactionEntity, Long> {

    /**
     * Returns a list of Transactions from repository filtered by account ID
     * @param accountId Filter value
     * @return List of Transactions
     */
    public List<TransactionEntity> findAllByAccountIdEquals(Integer accountId);

}
