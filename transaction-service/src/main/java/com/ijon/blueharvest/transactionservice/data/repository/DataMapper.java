package com.ijon.blueharvest.transactionservice.data.repository;

import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;

/**
 * Maps Domain Models to Repository Entities, and vice versa
 */
public class DataMapper {

    public static TransactionModel toDomain(TransactionEntity entity) {
        return new TransactionModel(entity.getId(), entity.getAccountId(), entity.getValue());
    }

    public static TransactionEntity toData(TransactionModel model) {
        return new TransactionEntity(model.getAccountId(), model.getValue());
    }

}
