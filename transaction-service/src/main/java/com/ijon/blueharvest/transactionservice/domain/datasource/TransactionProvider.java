package com.ijon.blueharvest.transactionservice.domain.datasource;

import com.ijon.blueharvest.transactionservice.data.repository.TransactionRepository;
import com.ijon.blueharvest.transactionservice.data.repository.TransactionSource;
import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;

import java.util.Arrays;
import java.util.List;

public class TransactionProvider {
    private static final List<TransactionDataSource> SOURCES = Arrays.asList(new TransactionSource());

    public TransactionProvider() {
    }

    /* Temporary solution for @Autowired Repository */
    public void addRepository(TransactionRepository repository) {
        for (TransactionDataSource source : SOURCES) {
            if (source instanceof TransactionSource) {
                ((TransactionSource) source).addRepository(repository);
            }
        }
    }

    /**
     * Stores Transaction data to repository and returns id of the new entry
     *
     * @param transaction Data to store
     * @return Id of the stored data
     */
    public Integer createTransaction(TransactionModel transaction) {
        for (TransactionDataSource s : SOURCES) {
            Integer result = s.createTransaction(transaction);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }

    /**
     * Returns a list of Transactions from repository filtered by account ID
     *
     * @param accountId Filter value
     * @return List of Transactions
     */
    public List<TransactionModel> getTransactionsForAccount(Integer accountId) {
        for (TransactionDataSource s : SOURCES) {
            List<TransactionModel> result = s.findTransactionsByAccountId(accountId);
            // Don't check next source if action successful
            if (result != null)
                return result;
        }

        return null;
    }
}
