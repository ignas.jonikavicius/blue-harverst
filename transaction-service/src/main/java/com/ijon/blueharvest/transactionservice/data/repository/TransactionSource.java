package com.ijon.blueharvest.transactionservice.data.repository;

import com.ijon.blueharvest.transactionservice.domain.datasource.TransactionDataSource;
import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;

import java.util.List;
import java.util.stream.Collectors;

public class TransactionSource implements TransactionDataSource {

    protected TransactionRepository transactionRepository;

    /* Temporary solution for @Autowired Repository */
    public void addRepository(TransactionRepository repository) {
        this.transactionRepository = repository;
    }

    @Override
    public Integer createTransaction(TransactionModel transaction) {
        return transactionRepository.save(DataMapper.toData(transaction)).getId();
    }

    @Override
    public List<TransactionModel> findTransactionsByAccountId(Integer accountId) {
        return transactionRepository.findAllByAccountIdEquals(accountId).stream()
                .map(DataMapper::toDomain)
                .collect(Collectors.toList());
    }
}
