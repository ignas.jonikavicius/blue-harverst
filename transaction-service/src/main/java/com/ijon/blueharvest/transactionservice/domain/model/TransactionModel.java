package com.ijon.blueharvest.transactionservice.domain.model;

public class TransactionModel {

    private Integer id;
    private Integer accountId;
    private double value;

    public TransactionModel(Integer id, Integer accountId, double value) {
        this.id = id;
        this.accountId = accountId;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
