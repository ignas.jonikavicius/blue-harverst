package com.ijon.blueharvest.transactionservice.domain.datasource;

import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;

import java.util.List;

public interface TransactionDataSource {

    /**
     * Stores Transaction data to repository and returns id of the new entry
     *
     * @param transaction Data to store
     * @return Id of the stored data
     */
    Integer createTransaction(TransactionModel transaction);

    /**
     * Returns a list of Transactions from repository filtered by account ID
     *
     * @param accountId Filter value
     * @return List of Transactions
     */
    List<TransactionModel> findTransactionsByAccountId(Integer accountId);
}
