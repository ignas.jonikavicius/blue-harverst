package com.ijon.blueharvest.transactionservice.data.repository;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_TRANSACTION")
public class TransactionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    protected Integer accountId;
    protected Double value;

    /**
     * Default constructor for JPA only.
     */
    protected TransactionEntity() {
    }

    public TransactionEntity(Integer accountId, Double value) {
        this.accountId = accountId;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

}
