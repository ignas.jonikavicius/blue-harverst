package com.ijon.blueharvest.transactionservice.controller;

import com.ijon.blueharvest.transactionservice.domain.commands.CreateTransactionCommand;
import com.ijon.blueharvest.transactionservice.domain.commands.RequestTransactionsByAccountIdCommand;
import com.ijon.blueharvest.transactionservice.domain.model.TransactionModel;
import com.ijon.blueharvest.transactionservice.data.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    TransactionRepository transactionRepository;

    @GetMapping("/account/{id}")
    public List<TransactionModel> getTransactionsForAccount(@PathVariable int id) {
        // TODO : add @PathVariable checks
        return new RequestTransactionsByAccountIdCommand(id).addRepository(transactionRepository).execute();
    }

    @PostMapping("")
    public ResponseEntity<Void> createTransaction(@RequestBody TransactionModel transaction) {
        // TODO : add @RequestBody validation
        // TODO : add success check on CreateTransactionCommand
        new CreateTransactionCommand(transaction).addRepository(transactionRepository).execute();
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
