drop table T_TRANSACTION if exists;
create table T_TRANSACTION
(
    ID         integer generated by default as identity (start with 1, increment by 1) primary key,
    ACCOUNT_ID integer not null,
    VALUE      double not null
)