package com.ijon.blueharvest.webservice.model;

public class UserModel {

    protected Integer id;
    protected String name;
    protected String surname;
    protected Integer accountId;
    protected Double balance;

    public UserModel() {
    }

    public UserModel(Integer id, String name, String surname, Integer accountId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.accountId = accountId;
        this.balance = 0.0;
    }

    public void addBalance(Double balance) {
        this.balance += balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
