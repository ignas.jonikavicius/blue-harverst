package com.ijon.blueharvest.webservice.service;

import com.ijon.blueharvest.webservice.model.AccountModel;
import com.ijon.blueharvest.webservice.model.OverviewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AccountService {

    public static final String ACCOUNT_SERVICE_URL = "http://account-service";

    @Autowired
    @LoadBalanced
    protected RestTemplate restTemplate;

    public HttpStatus createAccount(AccountModel account) {
        HttpEntity<AccountModel> request = new HttpEntity<>(account);
        ResponseEntity<String> uri = restTemplate.exchange(ACCOUNT_SERVICE_URL + "/accounts",
                HttpMethod.POST, request, String.class);

        return uri.getStatusCode();
    }

    public OverviewModel getOverview() {
        return restTemplate.getForObject(ACCOUNT_SERVICE_URL + "/overview", OverviewModel.class);
    }
}
