package com.ijon.blueharvest.webservice.controller;

import com.ijon.blueharvest.webservice.form.AccountForm;
import com.ijon.blueharvest.webservice.model.AccountModel;
import com.ijon.blueharvest.webservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AccountController {

    @Autowired
    AccountService accountService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields("customerID", "initialCredit");
    }

    @RequestMapping(value = "/open-account", method = RequestMethod.GET)
    public String viewOpenAccount(Model model) {
        model.addAttribute("accountForm", new AccountForm());
        return "open-account/form";
    }

    @RequestMapping("/do-open-account")
    public String doForm(Model model, AccountForm form, BindingResult result) {
        form.validate(result);

        if (result.hasErrors())
            return "open-account/form";

        AccountModel account = new AccountModel(0, Integer.parseInt(form.getCustomerID()), form.getInitialCredit());
        HttpStatus status = accountService.createAccount(account);

        if (status == HttpStatus.CREATED)
            return "open-account/success";
        else
            return "open-account/error";
    }
}
