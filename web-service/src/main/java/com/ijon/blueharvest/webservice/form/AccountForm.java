package com.ijon.blueharvest.webservice.form;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import java.util.Arrays;

public class AccountForm {

    private String customerID;
    private double initialCredit;

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public double getInitialCredit() {
        return initialCredit;
    }

    public void setInitialCredit(double initialCredit) {
        this.initialCredit = initialCredit;
    }

    public boolean validate(Errors errors) {

        if (!StringUtils.hasText(customerID))
            errors.rejectValue("customerID", "empty", "Customer ID should not be empty");

        // Quick validation to check if passed id exists.
        // TODO : use account-service for validating user's existence
        if (StringUtils.hasText(customerID) && !Arrays.asList(1, 2, 3, 4).contains(Integer.parseInt(customerID))) {
            errors.rejectValue("customerID", "invalid", "Customer ID does not exist");
        }

        return errors.hasErrors();
    }
}
