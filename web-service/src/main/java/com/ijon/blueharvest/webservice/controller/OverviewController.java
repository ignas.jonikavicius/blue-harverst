package com.ijon.blueharvest.webservice.controller;

import com.ijon.blueharvest.webservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OverviewController {

    @Autowired
    AccountService accountService;

    @RequestMapping("/overview")
    public String viewCustomerOverview(Model model) {
        model.addAttribute("overview", accountService.getOverview());
        return "overview";
    }
}
